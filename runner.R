source("/Users/alextank/Documents/granger_categorical/gen_dat.R")
source("/Users/alextank/Documents/granger_categorical/admm_nuclear.R")

N = 100
stepsize = 1
lambda = 1
ref_series = 1
p = 2
x = generate_categorical_series(N)
Nc = c(3,3)
P = lowrank_granger(x,stepsize,lambda,p,ref_series,Nc)