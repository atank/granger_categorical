#include <Rcpp.h>
using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar


void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}
void showValue_int(int x) {
    Rcout << "The value is " << x << std::endl;
}

void showMatrix(IntegerMatrix X) {
    Rcout << "Armadillo matrix is" << std::endl << X << std::endl;
}


double logsumexp(NumericVector x) {
  return(log(sum(exp(x - max(x)))) + max(x));
}

// [[Rcpp::export]]
NumericVector comp_denom_cpp(IntegerMatrix X, int q, int lag, ListOf<ListOf<NumericMatrix> > Z, IntegerVector Nc) {
  int mq = Nc[q - 1];
  int p = X.ncol();
  NumericVector exp_sums = NumericVector(mq);
  //showValue(X(0,0));
  for (int i = 0; i < mq;i++) {
    for (int j = 0; j < p;j++) {
      for (int k = 0;k < lag; k++) {
        exp_sums(i) += Z[j][k](i,X(lag-1-k,j) - 1);
        //showValue(X(0,j));
        //exp_sums(i) += Z[j][k](i,X(0,j) - 1);
      }
    }
    exp_sums(i) += Z[p][0](i,0);
  }
  //showValue(exp_sums[0]);
  double denom = logsumexp(exp_sums);
  NumericVector gradient = exp(exp_sums - denom);
  return(gradient);
}


// [[Rcpp::export]]
List GLM_gradient_cpp(IntegerMatrix X, ListOf<ListOf<NumericMatrix> > Z, int q,
ListOf<ListOf<NumericMatrix> > ZgradB, IntegerVector Nc, int lag, LogicalMatrix
series_on,IntegerVector indset) {
  //int N = X.nrow();
  int p = X.ncol();
  ListOf<ListOf<NumericMatrix> > Zgrad = clone(ZgradB);
  int Nt = indset.size();
  //for (int i = lag;i < N;i++) {
  for (int t = 0;t < Nt;t++) {
    int i = indset[t];
    IntegerMatrix f = X(Range(i-lag,i),Range(0,p-1));
    NumericVector gradient = comp_denom_cpp(f,q, lag, Z, Nc);
    //showValue(gradient(1));
    for (int j = 0;j < p;j++) {
        for (int k = 0;k < lag;k++) {
          if (series_on(j,k)) {  
            if (X(i-k-1,j) > 1) {
              for (int m = 0;m < (Nc[q-1] - 1) ;m++) {
                Zgrad[j][k](m,X(i-k-1,j)-1) += gradient(m);
              }
              if (X(i,q-1) < Nc[q - 1]) {
                Zgrad[j][k](X(i,q-1) - 1,X(i-k-1,j) - 1) -= 1;
              }
            }
          }
        }
      }
    for (int m = 0;m < (Nc[q - 1] - 1);m++) {
      Zgrad[p][0](m,0) += gradient(m); 
    } 
    if (X(i,q-1) < Nc[q - 1]) {
      Zgrad[p][0](X(i,q-1)-1,0) += -1;
    }
  }
  return(Zgrad);
}

// [[Rcpp::export]]
IntegerMatrix test1(IntegerMatrix X) {
  int i = 1;
  int lag = 1;
  int p = 3;
  IntegerMatrix sub = X(Range(i - lag,i),Range(0,p-1));
  return(sub);
}

// [[Rcpp::export]]
double loglikelihood_cpp(ListOf<ListOf<NumericMatrix> > Z,IntegerMatrix X, int
lag, IntegerVector Nc, int q,IntegerVector indset) {
  double loglik = 0;
  //int Ttrain = X.nrow();
  int p = X.ncol();
  int Nt = indset.size();

  //for (int i = lag;i < Ttrain;i++) {
  for (int j = 0;j < Nt; j++) {
    int i = indset[j];
    //showValue(i);
    //print(i);
    
    IntegerMatrix f = X(Range(i-lag,i),Range(0,p-1));
    NumericVector probs = comp_denom_cpp(f,q, lag, Z, Nc);
    //showValue(probs(0));
    loglik -= log(probs(X(i,q-1) - 1));
    //loglik += 1;
  }
  return(loglik);
}




