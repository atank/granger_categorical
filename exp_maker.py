from __future__ import division

import os
import numpy as np

from itertools import product

jfile = "BSG.sh"

N = [200]
meth = [1, 2]
gen_dat = [1,2,3]
lam = range(0,80,2)
nreps = 1
#nreps = 2
#nreps = 3
#nreps = 4
#nreps = 5

pen = [1,2]
ncat = [3]

basecmd = "mosrun -b -q -J29 nohup Rscript simulation_runner.R"
with open(jfile, 'w') as f:
    #loop over configurations for mLTD method
    par_prod = product(N,gen_dat,nreps,lam,ncat)
    for par in par_prod:
        n,gen,rep,la,m = par
        cmd = basecmd + " " + str(m) + " " + str(n) + " " + str(rep) + " " + "1" + " " + str(gen) + " " + "2" + " " + str(la)
        outfile = "logs/var_" + str(m) + "_" + str(n) + "_" + str(rep) + "_" + "1" + "_" + str(gen) + "_" + "2" + "_" + str(la) + ".log"
        cmd = cmd + " &> " + outfile + " " + "&"
        f.write(cmd + "\n")

    par_prod = product(N,gen_dat,nreps,lam,ncat,pen)
    for par in par_prod:
        n,gen,rep,la,m,pen_type = par
        cmd = basecmd + " " + str(m) + " " + str(n) + " " + str(rep) + " " + "2" + " " + str(gen) + " " + str(pen_type) + " " + str(la)
        outfile = "logs/var_" + str(m) + "_" + str(n) + "_" + str(rep) + "_" + "2" + "_" + str(gen) + "_" + str(pen_type) + "_" + str(la) + ".log"
        cmd = cmd + " &> " + outfile + " " + "&"
        f.write(cmd + "\n")

os.chmod(jfile, 0755)
