rng(18334190)
lams = [10000000000];
lams = [0 .1 1 10 20 30 60 80 100 150 200 300];
p = 6;
m = 2;
T = 500;
Ttest = 500;
nfolds = 1;
dim = m*ones(1,p+1);
X = zeros(T,1);
onzm = ones(m,1);
onzp = ones(p,1);
onzpm = ones(p*m,1);

zt = zeros(m,m,p);
%%%% for single transitions
temp = drchrnd(2*ones(m,1)',m);
for i = 1:p
    %temp = drchrnd(2*ones(m,1)',m);
    zt(:,:,i) = temp';
end
gammat = [.5 .2 .1 .1 .05 .05];

%%%%for smoothly changing transitions
temp1 = drchrnd(2*ones(m,1)',m);
temp2 = drchrnd(2*ones(m,1)',m);
comb = zeros(2,p);
comb(:,1) = [0 1];
comb(:,2) = [.2 .8];
comb(:,3) = [.4 .6];
comb(:,4) = [.6 .4];
comb(:,5) = [.8 .2];
comb(:,6) = [1 0];
for i = 1:p
    %temp = drchrnd(2*ones(m,1)',m);
    zt(:,:,i) = comb(1,i)*temp1' + comb(2,i)*temp2';
end


%for (i in 1:

%gammat(1,3) = .0001;
%gammat(1,:) = gammat(1,:)/sum(gammat(1,:));
X(1:p) = 1;
for t = (p+1):(T + Ttest)
    temp = zeros(m,1);
    for i = 1:p
        temp = temp + gammat(i)*zt(:,X(t-i),i);
    end
    disp(temp);
    disp([X(t-1) X(t-2) X(t-2)]);
    X(t) = randsample(m,1,true,temp);
end

%%cross validation
CVerr = zeros(length(lams),nfolds);
for lam = 1:length(lams)
   for q = 1:nfolds
       heldout = (p+q):nfolds:T;
       heldin = setdiff(p+1:T,heldout);
       Tq = length(heldin);
       Tqout = length(heldout);
        cvx_begin
            variable z(m,m,p) nonnegative;
            expression x(T - p)
            for t = (p + 1):(T)
                %t = heldin(ind);
                temp = 0;
                for i = 1:p
                    temp = temp + z(X(t),X(t-i),i);
                end
                x(t - p) = -log(temp);
            end
            expression ZV(m*m,p)
            for i = 1:p
                ZV(:,i) = vec(z(:,:,i));
            end
            minimize(sum(x) + lams(lam)*norm_nuc(ZV))
            subject to
                onzpm'*vec(squeeze(z(:,1,:))) == 1;
                for i = 1:p 
                    for j = 2:m
                        onzm'*z(:,j,i) - onzm'*z(:,1,i) == 0;
                    end
                end
        cvx_end
        ze = zeros(m,m,p);
        gammae = onzm'*squeeze(z(:,1,:));
        for i = 1:p
            ze(:,:,i) = z(:,:,i)/gammae(i);
        end

        %%%%compute prediction error%%%%
        total = 0;
        for t = (T + 1):(T + Ttest)
            temp = 0;
            for i = 1:p
                temp = temp + z(X(t),X(t-i),i);
            end
            total = total -log(temp);
        end
        CVerr(lam,q) = total;
   end
end

set(gca,'FontSize',20)

plot(log(lams(2:length(lams))),CVerr(2:length(lams)));
title('CV error for Smooth TM');
ylabel('pred -loglik');
xlabel('log(\lambda)');

