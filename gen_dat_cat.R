library("gtools")
library("rTensor")
library("caTools")
source("GLM_granger.R")


BIC_score <- function(logfinal,gmat_est,Nc,n,model) {
  p = length(Nc)
  msize = 0
  if (model == "mtd") {
    msize = 0
    for (i in 1:p) {
        msize = msize + Nc[i]  - 1
        for (j in 1:p) {
            msize = msize + ((Nc[i] - 1)*(Nc[j]) - Nc[i])*gmat_est[i,j]
        }
    }
    msize = msize + sum(gmat_est)
  }
  #if (model == "mtd") {
    # msize = msize + sum(gmat_est)
  #}
  #msize = msize + sum(Nc  - 1)
  return(list(2*sum(logfinal) + msize*log(n),msize,2*sum(logfinal) + 2*msize))
}

generate_cat_var <- function(A,N,p,m,lag) {
  quants_p = seq(0,1,by = 1/m)
  sdnoise = .1
  Y = matrix(0,N,p)
  X = matrix(0,N,p)
  Y[1,] = rnorm(p,0,1)
  for (i in 2:N) {
    Y[i,] = A %*% Y[i-1,] + rnorm(p,0,sdnoise)
  }
  for (i in 1:p) {
    quantiles = quantile(Y[,i],quants_p)
    for (j in 1:(length(quantiles) - 1)) {
      inds = (quantiles[j] <=  Y[,i]) & (Y[,i] <= quantiles[j + 1])
      Y[inds,i] = j
    }
  }
  return(Y)
}

generate_sparse_VAR <- function(spar_bern,p) {
  granger_mat = matrix(rbinom(p*p,1,spar_bern),p,p)
  A = matrix(0,p,p)
  for (i in 1:p) {
   for (j in 1:p) {
     if (granger_mat[i,j] == 1) {
      A[i,j] = .1 
     }
   } 
  }
  q = max(abs(eigen(A)$values))
  A = A/(q + .01)
  return(list(A,granger_mat))
}

generate_cat_dat_GLM <- function(Z,Ttrain,Ttest,lag,p,Nc) {
  m = min(Nc)
  X = matrix(0,Ttrain+Ttest,p)
  X[1:lag,] = matrix(sample(m,lag*p,replace=TRUE),lag,p)
  for (t in (lag+1):(Ttrain+Ttest)) {
    for (i in 1:p) {
      pveci = comp_denom(as.matrix(X[t:(t-lag),]),i,lag,Z[[i]],Nc)
      #print(pveci)
      X[t,i] = sample(Nc[i],1,replace=TRUE,pveci)
    }
  }
  return(X)
} 

init_Z_list <- function(p,lag,Nc,q) {
  Ze = list()
  for (i in 1:p) {
    Ze[[i]] = list()
    for (k in 1:lag) {
      Ze[[i]][[k]] = matrix(rnorm(Nc[q]*Nc[i],0,.3),Nc[q],Nc[i])
      Ze[[i]][[k]][,1] = 0
      Ze[[i]][[k]][Nc[q],] = 0
    }
  }
  Ze[[p+1]] = list();Ze[[p+1]][[1]] = matrix(0,Nc[q],Nc[q])
  return(Ze)
}

generate_sparse_GLM_granger <- function(Nc,total_variation_thresh,spar_bern,lag) {
  Z = list()
  p = length(Nc)
  granger_mat = matrix(rbinom(p*p,1,spar_bern),p,p)
  for (i in 1:p) {
    Z[[i]] = list()
    for (q in 1:p) {
      Z[[i]][[q]] = list()
      for (j in 1:lag) {
        if (granger_mat[i,q] == 0) {
            Z[[i]][[q]][[j]] = matrix(0,Nc[i],Nc[q])
        }
        if (granger_mat[i,q] == 1) {
          Z[[i]][[q]][[j]] = matrix(rnorm(Nc[i]*Nc[q],0,sd = 1),Nc[i],Nc[q])
          Z[[i]][[q]][[j]][abs(Z[[i]][[q]][[j]]) < .2] =  .2
          Z[[i]][[q]][[j]][,1] = 0
          Z[[i]][[q]][[j]][Nc[i],] = 0
        }
      }
    }
    Z[[i]][[p+1]] = list()
    Z[[i]][[p+1]][[1]] = matrix(0,Nc[i],Nc[i])
    Z[[i]][[p+1]][[1]][,1] = rnorm(Nc[i],0,sd = .1)
    Z[[i]][[p+1]][[1]][Nc[i],] = 0
  }
  return(list(Z,granger_mat))
}


compute_sparsity <- function(Z,p,lag,thresh) {
  gammaest = rep(0,p)
  for (i in 1:p) {
    for (k in 1:lag) {
      ind = (k-1)*p + i
      gammaest[ind] = norm(Z[[i]][[k]]) 
    }
  }
  return(list((gammaest > thresh)*1,gammaest))
}

compute_sparsity_norm <- function(Z,p,lag,thresh,mtype) {
    gammaest = rep(0,p) 
    for (i in 1:p) {
        for (k in 1:lag) {
            ind = (k-1)*p + i
            if (mtype == "glm") {
                dim1 = dim(Z[[i]][[k]])[1]
                dim2 = dim(Z[[i]][[k]])[2]
                gammaest[ind] = norm(Z[[i]][[k]])/(sqrt(dim1*dim2))
            }
            if (mtype == "mtd") {
                gammaest[ind] = sum(Z[[i]][[k]][,1])
            }
        }
    }
    return(list((gammaest > thresh)*1,gammaest))
}

compute_probability_tensor_MTD <- function(Z,lag,Nc,q) {
  p = length(Nc)
  p_tensor = array(0,c(Nc[q],rep(Nc,lag)))
  for (i in 1:p) {
    for (k in 1:lag) {
      temp = attributes(k_unfold(as.tensor(p_tensor),p*(k-1) + i + 1))$data
      z_temp = array(0,dim(temp))
      ncol_temp = dim(temp)[2]
      
      for (j in 1:dim(temp)[1]) {
        #z_temp[j,] = rep(z_table[,j,i,k],ncol_temp/length(z_table[,j,i,k]))
        z_temp[j,] = rep(Z[[i]][[k]][,j],ncol_temp/length(Z[[i]][[k]][,j]))
      }
      temp = temp + z_temp
      p_tensor = attributes(k_fold(temp,p*(k-1) + i + 1,dim(p_tensor)))$data
    }
  }
  temp = attributes(k_unfold(as.tensor(p_tensor),1))$data
  for (j in 1:dim(temp)[2]) {
    temp[,j] = temp[,j] + Z[[p+1]][[1]][,1]
  }
  p_tensor = attributes(k_fold(temp,1,dim(p_tensor)))$data
}

compute_roc <- function(A,Aest) {
  areNzero = which(A != 0);
  arezero = which(A == 0)
  Amax = max(Aest)
  steps = unique(sort(as.vector(Aest)))
  steps = c(steps, max(steps) + 100)
  roc = matrix(0,2,length(steps))
  for (i in 1:length(steps)) {
    #print(i)
    nozero = which(Aest >= steps[i])
    zero = which(Aest < steps[i])
    
    roc[1,i] = length(intersect(nozero,areNzero))/length(areNzero)
    roc[2,i] = length(intersect(nozero,arezero))/length(arezero)
  }
  
  ###compute area under the ROC curve 
  auc = trapz(rev(roc[2,]),rev(roc[1,]))
  return(list(roc,auc))
}

compute_probability_tensor_GLM <- function(Z,lag,Nc,q) {
  p = length(Nc)
  p_tensor = array(0,c(Nc[q],rep(Nc,lag)))
  for (i in 1:p) {
    for (k in 1:lag) {
      temp = attributes(k_unfold(as.tensor(p_tensor),p*(k-1) + i + 1))$data
      z_temp = array(0,dim(temp))
      ncol_temp = dim(temp)[2]
      
      for (j in 1:dim(temp)[1]) {
        #z_temp[j,] = rep(z_table[,j,i,k],ncol_temp/length(z_table[,j,i,k]))
        z_temp[j,] = rep(Z[[i]][[k]][,j],ncol_temp/length(Z[[i]][[k]][,j]))
      }
      temp = temp + z_temp
      p_tensor = attributes(k_fold(temp,p*(k-1) + i + 1,dim(p_tensor)))$data
    }
  }
  temp = attributes(k_unfold(as.tensor(p_tensor),1))$data
  for (j in 1:dim(temp)[2]) {
    temp[,j] = temp[,j] + Z[[p+1]][[1]][,1]
    ltemp = logSumExp(temp[,j])
    temp[,j] = exp(temp[,j] - ltemp)
  }
  p_tensor = attributes(k_fold(temp,1,dim(p_tensor)))$data
  return(p_tensor)
}

calculate_count_tensor <- function(x,ref_series,Nc,lag) {
  Cn = array(0,c(Nc[ref_series],Nc))
  tots = array(0,c(Nc[ref_series],Nc))
  N = dim(x)[2]
  for (i in (lag+1):N) {
    indices = c(x[ref_series,i],x[,i-1])
    Cn[t(indices)] = Cn[t(indices)] + 1
    for (j in 1:Nc[ref_series]) {
      indices = c(j,x[,i-1])
      tots[t(indices)] = tots[t(indices)] + 1
    }
  }
  return(list(Cn/tots,Cn))
}
#######generate categorical time series according to an MTD model####
generate_cat_dat <- function(p_table,gamma,Ttrain,Ttest,lag,m) {
  p = dim(gamma)[1]
  X = matrix(0,Ttrain+Ttest,p)
  X[1:lag,] = matrix(sample(m,lag*p,replace=TRUE),lag,p);
  for (t in (lag + 1):(Ttrain+Ttest)) {
    for (i in 1:p) {
      temp = rep(0,m)
      for (j in 1:p) {
        for (k in 1:lag) {
          gamma_ind = (k - 1)*p + j
          temp = temp + gamma[i,gamma_ind]*p_table[,X[t - k,j],j,k,i]
        }
      }
      X[t,i] = sample(m,1,replace=TRUE,temp)
    }
  }
  return(X)
}

compute_probability_tensor_p3 <- function(z_table,lag) {
  p = dim(z_table)[3]
  m = dim(z_table)[1]
  print(dim(z_table))
  p_tensor = array(0,rep(m,lag*p+1))
  print(dim(p_tensor))  
  for (i in 1:m) {
    for (j in 1:m) {
      for (q in 1:m) {
        for (l in 1:m) {
          p_tensor[i,j,q,l] = z_table[i,j,1,1] + z_table[i,q,2,1] + z_table[i,l,3,1]
        }
      }
    }
  }
  return(p_tensor)
}

compute_probability_tensor <- function(z_table,lag) {
  p = dim(z_table)[3]
  m = dim(z_table)[1]
  print(dim(z_table))
  p_tensor = array(0,rep(m,lag*p+1))
  print(dim(p_tensor))
  for (i in 1:p) {
    for (k in 1:lag) {
      temp = attributes(k_unfold(as.tensor(p_tensor),p*(k-1) + i + 1))$data
      z_temp = array(0,dim(temp))
      ncol_temp = dim(temp)[2]
      
      for (j in 1:dim(temp)[1]) {
        z_temp[j,] = rep(z_table[,j,i,k],ncol_temp/length(z_table[,j,i,k]))
      }
      temp = temp + z_temp
      p_tensor = attributes(k_fold(temp,p*(k-1) + i + 1,dim(p_tensor)))$data
    }
  }
  
  
  
  return(p_tensor)
}




log_probability_MTD <- function(X,z_table,q,lag,p,lambda,pentype) {
  eps = .00000001
  Nt = dim(X)[1]
  #print(z_table)
  logp = 0
  for (t in (lag + 1):Nt) {
    temp = 0
    for (i in 1:p) {
      for (k in 1:lag) {
        temp = temp + z_table[[i]][[k]][X[t,q],X[t-k,i]]
      }
    }
    temp = temp + z_table[[p+1]][[1]][X[t,q]]
    logp = logp - log(temp + eps)
  }
  print(logp)
  if (pentype == "group_lasso") { 
    for (i in 1:p) {
      for (k in 1:lag) {
        logp = logp + lambda*(norm(as.matrix(z_table[[i]][[k]]),'f'))
      }
    }
  }
  if (pentype == "ridge") {
    for (i in 1:p) {
      for (k in 1:lag) {
        logp = logp + lambda*(norm(as.matrix(z_table[[i]][[k]]),'f'))^2
      }
    }
  }

  if (pentype == "gcgfl") { 
    for (i in 1:p) {
      for (k in 1:lag) {
        z_m = apply(z_table[[i]][[k]],1,mean)
        logp = logp + lambda*(norm(as.matrix(z_table[[i]][[k]] - z_m),'f'))
      }
    }
  }
  return(logp)
}

generate_sparse_MTD <- function(spar_gam,spar_p_table,p,m,lag,sparse_bern,total_variation_thresh) {
  gamma = rdirichlet(p,rep(spar_gam,p*lag))
  for (i in 1:p) {
    GC = sample(2,p,replace=TRUE,c(1-sparse_bern,sparse_bern)) - 1
    GC[i] = 1
    temp = gamma[i,]*GC
    gamma[i,] = temp/sum(temp)
  }
  p_table = array(0,c(m,m,p,lag,p))
  z_table = array(0,c(m,m,p,lag,p))
  for (i in 1:p) {
    for (j in 1:p) {
      for (k in 1:lag) {
        total_var = 0
        while (total_var < total_variation_thresh) {
          p_table[,,j,k,i] = t(rdirichlet(m,rep(spar_p_table,m)))
          z_table[,,j,k,i] = p_table[,,j,k,i]*gamma[i,p*(k - 1) + j]
          sum_abs = 0
          iter = 1;
          for (b in 1:m) {
            for (c in 1:m) {
              if (c > b) {
                #print(p_table[,b,j,k,i])
                #print(p_table[,c,j,k,i])
                sum_abs = sum_abs + mean(abs(p_table[,b,j,k,i] - p_table[,c,j,k,i]));
                iter = iter + 1
              }
            }
          }
          total_var = sum_abs/iter
        }
      }
    }
  }
  z_table_con = list()
  for (q in 1:p) {
    z_table_con[[q]] = list()
    int = rep(0,m)
    for (i in 1:p) {
      z_table_con[[q]][[i]] = list()
      for (k in 1:lag) {
        minz = apply(z_table[,,i,k,q],1,min)
        int = int + minz
        z_table_con[[q]][[i]][[k]] = z_table[,,i,k,q] - minz
      }
    }
    z_table_con[[q]][[p+1]] = list();z_table_con[[q]][[p+1]][[1]] = matrix(0,m,m);
    z_table_con[[q]][[p+1]][[1]][,1] = int
  }
  
  return(list(p_table,gamma,z_table,z_table_con))
}

generate_random_MTD <- function(spar_gam,spar_p_table,p,m,lag) {
  gamma = rdirichlet(p,rep(spar_gam,p*lag))
  p_table = array(0,c(m,m,p,lag,p))
  z_table = array(0,c(m,m,p,lag,p))
  for (i in 1:p) {
    for (j in 1:p) {
      for (k in 1:lag) {
        p_table[,,j,k,i] = t(rdirichlet(m,rep(spar_gam,m)))
        z_table[,,j,k,i] = p_table[,,j,k,i]*gamma[i,p*(k - 1) + j]
      }
    }
  }
  return(list(p_table,gamma,z_table))
}
