function MTD_convex(seed,p_tp,T)
%seed = 1;
%p_tp = 2;
warning('off','all');
seedb = 190*seed;
rng(seedb)
p = 6;
m = 2;
%%%FART%%%%MTD
%T = 500;
Ttest = 500;
lams = [0,1,10,50,80,100,200,300,400,500];
lams_mf = [0,1,10,15,20,25,30,50,60,400];
lams_nuc = [0,1,10,15,20,30,50,60,200,400];
%lams_mf = [0,100];
%lams = lams_mf;
%lams_mf = 20;
%lams = 1;
%lams = [0,10];
%lams = 100;
dim = m*ones(1,p+1);
X = zeros(T,p);
onzpm = ones(p*m,1);
thresh = .3;
if (p_tp == 1) 
    pen_type = 'group_mean_fused';
end
if (p_tp == 2)
    pen_type = 'group_lasso';
end
if (p_tp == 3)
    pen_type = 'nuc';
end
    
%pen_type = 'group_lasso';

filename = strcat('./results/',pen_type,int2str(seed),int2str(T),'.mat');

zt = zeros(m,m,p,p);
for q = 1:p
    for i = 1:p
        sum_abs = 0;
        while (sum_abs < thresh) 
            temp = drchrnd(.9*ones(m,1)',m); temp = temp';
            sum_abs = 0;
            iter = 0;
            for b = 1:m
                for c = (b + 1):m
                    sum_abs = sum_abs + mean(abs(temp(:,b) - temp(:,c)));
                    iter = iter + 1;
                end
            end
            sum_abs = sum_abs/iter;
        end
        zt(:,:,i,q) = temp;
    end
end
gammat = drchrnd(11.5*ones(p,1)',p);
for j = 1:p
    mask = randsample(2,p,true,[.7 .3]) - 1;
    mask(j) = 1;
    gammat(j,:) = gammat(j,:).*mask'/(sum(gammat(j,:).*mask'));
end

%gammat(1,3) = .0001;
%gammat(1,:) = gammat(1,:)/sum(gammat(1,:));
X(1,:) = ones(1,p);


for t = 2:(T+Ttest)
    for j = 1:p
        temp = zeros(m,1);
        for i = 1:p
            temp = temp + gammat(j,i)*zt(:,X(t-1,i),i,j);
        end
        X(t,j) = randsample(m,1,true,temp);
    end
end

Cn = zeros(dim);
q = 1; %index of fixed series
onzm = ones(m,1);
onzp = ones(p,1);
%%%create list of observed pairs
gammainf = zeros(p,p,length(lams));

CVerr = zeros(length(lams),1);
false_pos = zeros(length(lams),1);
true_pos = zeros(length(lams),1);
for lam = 1:length(lams)
    disp(lam);
    %for q = 1:p
    for q = 1:p
        %for t = 1:(T - 1)
            %[X(t+1,q),X(t,1),X(t,2)]
        %    Cn(X(t+1,q),X(t,1),X(t,2),X(t,3)) = Cn(X(t+1,q),X(t,1),X(t,2),X(t,3)) + 1;
        %end
        cvx_begin quiet
            variable z(m,m,p) nonnegative;
            variable zm(m,p) nonnegative;
            %variable gammae(p) nonnegative;
            expression x(T-1)
            for t = 1:(T - 1)
                temp = 0;
                for i = 1:p
                    temp = temp + z(X(t+1,q),X(t,i),i);
                end
                x(t) = -log(temp);
            end
        %     expression total(1)
        %     total = 0;
        %     for i = 1:m
        %         for j = 1:m
        %             for k = 1:m
        %                 for w = 1:m
        %                     ids = [j,k,w];
        %                     temp = 0;
        %                     for g = 1:p
        %                         temp = temp + z(i,ids(g),g);
        %                     end
        %                     total = total - Cn(i,j,k,w)*log(temp);
        %                 end
        %             end
        %         end
        %     end

            expression pen(p)
            for i = 1:p
                %pen(i) = norm(z(:,m+1,i),1.2);
                pen(i) = norm(vec(z(:,:,i)));
            end
            
            expression pen_nuc(p)
            for i = 1:p
                pen_nuc(i) = norm_nuc(z(:,:,i));
            end
            
            expression pen_mf(p)
            for i = 1:p 
                pen_mf(i) = norm(vec(z(:,:,i)) - repmat(zm(:,i),m,1));
            end
            
            %%%no sparsity
            %minimize(sum(x))
            if (strcmp(pen_type,'group_lasso'));
                minimize(sum(x) + lams(lam)*sum(pen));
            end
            if strcmp(pen_type,'group_mean_fused');
                minimize(sum(x) + lams_mf(lam)*sum(pen_mf));
            end
            if strcmp(pen_type,'nuc');
                minimize(sum(x) + lams_nuc(lam)*sum(pen_nuc));
            end
            
            
            %minimize(total)
            %minimize(sum(pen))
            %%%sparsity
            %minimize(sum(x) + lam*sum(pen))
            subject to
        %gammae >= .0001;
                %gammae'*onzp == 1;
                onzpm'*vec(squeeze(z(:,1,:))) == 1;
                %gamma > 0;
                %z > 0;
                for i = 1:p 
                    for j = 2:m
                        onzm'*z(:,j,i) - onzm'*z(:,1,i) == 0;
                    end
                end
        cvx_end
        ze = zeros(m,m,p);

        gammae = onzm'*squeeze(z(:,1,:));
        gammainf(:,q,lam) = gammae; 
        for i = 1:p
            ze(:,:,i) = z(:,:,i)/gammae(i);
            ze_f(:,:,i,q,lam) = z(:,:,i)/gammae(i);
        end
    
        %%%compute predictive log likelihood
        for t = (T + 1):(T + Ttest)
            temp = 0;
            for i = 1:p
                temp = temp + z(X(t,q),X(t-1,i),i);
            end
            CVerr(lam) = CVerr(lam) - log(temp);
        end
    end
    
    [false_pos(lam), true_pos(lam)] = compute_ROC(ze_f(:,:,:,:,lam),gammat,gammainf(:,:,lam)',pen_type,p,m);
end

fptp = [false_pos';true_pos'];
save(filename,'fptp','CVerr','gammat','gammainf','ze_f');

%set(gca,'FontSize',20)

%plot(log(lams(2:length(lams))),CVerr(2:length(lams),1));
%title('CV error for Mult MTD');
%ylabel('pred -loglik');
%xlabel('log(\lambda)');
%xlim([0 6])

%  %%plot connectivity matrix%%%
% figure
% data = (gammat < .0001)*1.000001;
% imagesc(data);
% colormap(gray)
% %addXlabel(hmo,'True','FontSize',12)
% %subplot(2,1,2)
% %data = (gammat > .0001)*1.000001;
% %imagesc(data);
% 
% 
% for i = 1:3
%     for j = 1:3
%         ind = (i - 1)*3 + j;
%         subplot(3,4,ind + 3);
%         data = (gammainf(:,:,ind)' < .01)*1.000001;
%         imagesc(data);
%         %addXLabel(hmo, strct('\lambda = ',lams(lam)), 'FontSize', 12)
%     end
%end




% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%construct probability tensor for 2D
% condmate = zeros(m,m,m);
% condmatt = zeros(m,m,m);
% for i = 1:m
%     for j = 1:m
%         condmate(:,i,j) = gammae(1)*ze(:,i,1) + gammae(2)*ze(:,j,2);
%         condmatt(:,i,j) = gammat(1,1)*zt(:,i,1) + gammat(1,2)*zt(:,j,2);
%     end
% end
% 
% %construct probability tensor for 3D
% condmate = zeros(m,m,m,m);
% condmatt = zeros(m,m,m,m);
% for i = 1:m
%     for j = 1:m
%         for k = 1:m
%             condmate(:,i,j,k) = gammae(1)*ze(:,i,1) + gammae(2)*ze(:,j,2) + gammae(3)*ze(:,k,3);
%             condmatt(:,i,j,k) = gammat(1,1)*zt(:,i,1) + gammat(1,2)*zt(:,j,2) + gammat(1,3)*zt(:,k,3);
%         end
%     end
% end
end


