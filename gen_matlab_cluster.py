from __future__ import division
import os
import numpy as np
from itertools import product

jfile = "GC_mosix.sh"
penalty = [1,2,3]
seed = range(1,80,1)
basecmd = 'mosrun -b -e -q nohup matlab -nodisplay -nodesktop -nosplash -r "run MTD_convex'
with open(jfile, 'w') as f:
    f.write("#!/bin.bash" + "\n")
    par_prod = product(penalty,seed)
    for par in par_prod:
        pen,see = par
        cmd = basecmd + "(" + str(pen) + "," + str(see) + '); exit;"' 
        outfile = "./logs/" + str(pen) + str(see)  + ".log"
        cmd = cmd + " &> " + outfile
        f.write(cmd + "\n") 