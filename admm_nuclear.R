library("rTensor")
admm_granger <- function(x,rho,lambda,p,ref_series,Nc,type) {
  #####begin admm algorithm
  eps_abs = .00001
  eps_rel = .00001
  output = calculate_count_matrix(t(x),ref_series,Nc);
  Cn = output[[2]]
  pp = prod(dim(Cn))
  Cn_p = output[[1]]
  thresh = .001
  diff = 1
  Zglobal = output[[1]]
  Zglobal = array(abs(rnorm(8,0,1)),c(2,2,2))
  #initializeZ(Cn,eps)
  Z = list()
  U = list()
  z = list()
  for (i in 1:p) {
    Z[[i]] = Zglobal
    U[[i]] = array(0,dim(Zglobal))
    Cn_pi = attributes(k_unfold(as.tensor(Cn_p),i+1))$data
    z[[i]] = apply(Cn_pi,2,mean)
  }
  Uproj = array(0,dim(Zglobal))
  Zproj = Zglobal
  iter = 1
  #while (iter < 1000) {
  primal_cond = FALSE
  dual_cond = FALSE
  #while (!primal_cond & !dual_cond) {
  while (iter < 5000) {
    iter = iter + 1
    print(iter)
    #update zs
    Z_old = Z
    Zglobal_old = Zglobal
    Zproj_old = Zproj
    Zglobal = update_global(x,Z,U,Zproj,Uproj,rho,Cn)
    if (type == "nuclear" || type == "gnuclear") {Z = update_local_nuclear(Zglobal,U,rho,p,lambda,type)}
    if (type == "cgfl" || type == "gcgfl") {
      output = update_local_cgfl(Zglobal,U,rho,p,lambda,z,type,Nc)
      Z = output[[1]]
      z = output[[2]]
    }
    Zproj = update_proj(Zglobal,Uproj,Nc,ref_series)
    for (i in 1:p) {
      U[[i]] = U[[i]] + (Zglobal - Z[[i]])
    }
    Uproj = Uproj + (Zglobal - Zproj)
  
    ###stopping criteria###
    s = norm(rho*as.matrix(Zglobal - Zglobal_old),'f')
    r = rep(0,p+1)
    s = rep(0,p+1)
    eps_primal = rep(0,p+1)
    eps_dual = rep(0,p + 1)
    primal_cond = 1
    for (i in 1:p) {
      r[i] = norm(as.matrix(Z[[i]] - Zglobal),'f')
      eps_primal[i] = sqrt(pp)*eps_abs + eps_rel*max(norm(as.matrix(Z[[i]]),'f'),norm(as.matrix(Zglobal),'f'))
      eps_dual[i] = sqrt(pp)*eps_abs + eps_rel*norm(rho*as.matrix(U[[i]]),'f')
      primal_cond = primal_cond*(r[i] < eps_primal[i])*1
      s[i] = norm(rho*as.matrix(Z_old[[i]] - Z[[i]]))
      dual_cond = dual_cond*(s[i] < eps_dual[i])*1
    }
    s[p + 1] = norm(rho*as.matrix(Zproj - Zproj_old[[i]]),'f')
    eps_dual[i] = sqrt(pp)*eps_abs + eps_rel*norm(rho*as.matrix(Uproj),'f')
    r[p + 1] = norm(as.matrix(Zproj - Zglobal),'f')
    eps_primal[p + 1] = sqrt(pp)*eps_abs + eps_rel*max(norm(as.matrix(Zproj),'f'),norm(as.matrix(Zglobal),'f'))
    primal_cond = primal_cond*(r[p+1] < eps_primal[p+1])*1
    dual_cond = dual_cond*(s[p+1] < eps_dual[p+1])*1
    #eps_dual[p + 1] = sqrt(pp)*eps_abs + eps_rel*norm(rho*as.matrix(Uproj),'f')
    #eps_dual_min = min(eps_dual)
    #dual_cond = (s < eps_dual[p+1])*1
    #dual_cond = (s < eps_dual_min)*1    
  }
  return(list(Zglobal,Z,Zproj,Cn_p,z))
}

compute_differences <- function(Zglobal_old,Zglobal,Z_old,Z,Zproj_old,Zproj) {
  diff = rep(0,p+2)
  for (i in 1:p) {
    diff[i] = norm(as.matrix(Z_old[[i]] - Z[[i]]))
  }
  diff[i + 1] = norm(as.matrix(Zglobal - Zglobal_old))
  diff[i + 2] = norm(as.matrix(Zproj - Zproj_old))
  return(max(diff))
}

compute_differencesu <- function(Zglobal,Z,Zproj) {
  diffs = rep(0,p+1)
  for (i in 1:p) {
    diffs[i] = norm(as.matrix(Zglobal - Z[[i]]))
  }
  diffs[p+1] = norm(as.matrix(Zglobal - Zproj))
  return(max(diffs))
}


update_proj <- function(Uproj,Zglobal,Nc,ref_series) {
  Zmode1 = attributes(k_unfold(as.tensor(Zglobal + Uproj),1))$data;
  Zmat = matrix(0,dim(Zmode1)[1],dim(Zmode1)[2])
  ones = rep(1,Nc[ref_series])
  for (i in 1:(dim(Zmode1)[2])) {
    z = Zmode1[,i]
    Zmat[,i] = z - ((ones %*% z - 1)/length(ones))*ones
    #print(sum(Zmat[,i]))
    #print("fat")
  }
  Zproj = attributes(k_fold(Zmat,1,dim(Zglobal)))$data
  return(Zproj)
}

calculate_count_matrix <- function(x,ref_series,Nc) {
  Cn = array(0,c(Nc[ref_series],Nc))
  tots = array(0,c(Nc[ref_series],Nc))
  for (i in 2:N) {
    indices = c(x[ref_series,i],x[,i-1])
    Cn[t(indices)] = Cn[t(indices)] + 1
    for (j in 1:Nc[ref_series]) {
      indices = c(j,x[,i-1])
      tots[t(indices)] = tots[t(indices)] + 1
    }
  }
  return(list(Cn/tots,Cn))
}

update_global_nuclear <- function(x,Z,U,Zproj,Uproj,rho,Cn) {
  zu = as.vector(-Zproj + Uproj)
  cn = as.vector(Cn)
  for (i in 1:p) {
    zu = zu + as.vector(- Z[[i]] + U[[i]])
  }
  zglobal_vec = (-rho*zu+sqrt((rho^2)*(zu^2) + 4*rho*(p+1)*cn))/(2*rho*(p+1))
  #print(zglobal_vec)
  Zglobal = array(zglobal_vec,dim(Zproj))
  return(Zglobal)
}

update_local_nuclear <- function(Zglobal,U,rho,p,lambda,type) {
  Z = list()
  for (i in 1:p) {
    mat = attributes(k_unfold(as.tensor(Zglobal + U[[i]]),i+1))$data
    if (type == "nuclear") {outmat = soft_thresh_singular(mat,rho,lambda)}
    if (type == "gnuclear") {outmat = group_soft_thresh_singular(mat,rho,lambda)}
    Z[[i]] = attributes(k_fold(outmat,i+1,dim(Zglobal)))$data
  }
  return(Z)
}

update_local_cgfl <- function(Zglobal,U,rho,p,lambda,z,type,Nc) {
  Z = list()
  thresh = .001
  for (i in 1:p) {
    mat = attributes(k_unfold(as.tensor(Zglobal + U[[i]]),i+1))$data
    outmat = soft_thresh_cgfl(mat,rho,lambda,type,thresh,z[[i]],Nc[i])
    Z[[i]] = attributes(k_fold(outmat[[1]],i+1,dim(Zglobal)))$data
    z[[i]] = outmat[[2]]
  }
  return(list(Z,z))
}

soft_thresh_cgfl <- function(mat,rho,lamda,type,thresh,z,Nc) {
  diff = 10
  beta = 0*mat
  ones = rep(1,length(mat[,1]))
  while (diff > thresh) {
    z_old = z
    beta_old = beta
    if (type == "cgfl") {
      for (i in 1:Nc) {
        beta[,i] = group_soft_threshold(mat[i,] - z,lambda/rho)
      }
    }
    if (type == "gcgfl") {
        beta = group_soft_threshold(mat - ones %*% t(z),lambda/rho) 
    }
    diff = max(norm(as.matrix(z_old - z)),norm(as.matrix(beta_old - beta),'f'))
  }
  return(list(as.matrix(beta + ones %*% t(z)),z))
}

group_soft_threshold <- function(x,lam) {
  temp = (1 - lam/norm(as.matrix(x),'f'))
  if (temp > 0) {return(temp*x)}
  if (temp <= 0) {return(0*x)}
}

group_soft_thresh_singular <- function(Z,rho,lambda) {
  o = svd(Z)
  nsv = rep(0,length(o$d))
  nsv[2:length(o$d)] = group_soft_threshold(o$d[2:length(o$d)],lambda/rho)
  nsv[1] = group_soft_threshold(o$d[1],lambda/rho)
  return(o$u %*% diag(nsv) %*% t(o$v))
}

soft_thresh_singular <- function(Z,rho,lambda) {
  o = svd(Z)
  nsv = rep(0,length(o$d))
  print(o$d)
  for (j in 1:length(o$d)) {
    nsv[j] = group_soft_threhold(o$d[j],lambda/rho)
    #if (o$d[j] > lambda/rho) {
    #  nsv[j] = o$d[j] - lambda/rho
    #}
    #if (o$d[j] < -lambda/rho) {
    #  o[j] = o$d[j] + lambda/rho
    #}
    #if (o$d[j] < lambda/rho & o$d[j] > -lambda/rho) {
    #  nsv[j] = 0
    #  print("HERERERERE")
    #}
  }
  return(o$u %*% diag(nsv) %*% t(o$v))
}
