#include <Rcpp.h>
using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

// [[Rcpp::export]]
int timesTwo(int x) {
   return x * 2;
}

#include <Rcpp.h>
using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar


void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}
void showValue_int(int x) {
    Rcout << "The value is " << x << std::endl;
}

void showMatrix(IntegerMatrix X) {
    Rcout << "Armadillo matrix is" << std::endl << X << std::endl;
}

double fnorm(NumericMatrix X) {
  int n = X.nrow();
  int m = X.ncol();
  double total = 0;
  for (int i = 0;i < n;i++) {
    for (int j = 0;j < m;j++) {
      total += X(i,j)*X(i,j);
    }
  }
  return(sqrt(total));
}

// [[Rcpp::export]]
double log_probability_MTD_cpp(IntegerMatrix X, ListOf<ListOf<NumericMatrix> > Z,int q, int lag, int p, double lambda, String pen_type,IntegerVector inds) {
  //double eps = .0000000000001;
  double eps = .000001;
  //double eps = .0001;
  int Ninds = inds.size();
  double logp = 0;
  for (int indst = 0; indst < Ninds;indst++) {
    int t = inds(indst);
    double temp = 0;
    for (int i = 0;i < p;i++) {
      for (int k = 0;k < lag;k++) {
        temp += Z[i][k](X(t,q - 1)-1,X(t-k - 1,i)-1);
      }
    }
    temp += Z[p][0](X(t,q - 1)-1,0);
    logp -= log(temp + eps);
  }
  if (pen_type == "l1") {
    for (int i = 0;i < p;i++) {
      for (int k = 0;k < lag;k++) {
        int n = Z[i][k].ncol();
        int m = Z[i][k].nrow();
        double nd = (double) n;
        for (int j = 0;j < m;j++) {
          for (int b = 0;b < n; b++) {
            logp += (lambda/nd)*Z[i][k](j,b);
          }
        }
      }
    }
  }
  if (pen_type == "ridge") {
    for (int i = 0; i < p;i++) {
      for (int k = 0;k < lag;k++) {
        //z_grad[i][k] += mult_mat(Z[i][k],lambda);
        double ff = fnorm(Z[i][k]);
        logp += lambda*ff*ff;
      }
    }
  }
  if (pen_type == "group_lasso") {
    for (int i = 0;i < p; i++) {
      for (int k = 0;k<lag;k++) {
        double norm = fnorm(Z[i][k]);
        logp += lambda*norm;
      }
    }
  }
  return(logp);
}


NumericMatrix mult_mat(NumericMatrix X,double y) {
  int n = X.nrow();
  int m = X.ncol();
  NumericMatrix Y(n,m);
  for (int i = 0;i < X.nrow();i++) {
    for (int j = 0;j < X.ncol();j++) {
      Y(i,j) = y*X(i,j);
    }
  }
  return(Y);
}



// [[Rcpp::export]]
List MTD_gradient_cpp(IntegerMatrix X, ListOf<ListOf<NumericMatrix> > Z, int q, ListOf<ListOf<NumericMatrix> > z_grad_in, IntegerVector Nc, int lag, String pen_type,double lambda,IntegerVector inds) {
  int p = Nc.size();
  double eps = .0000001;
  int Ninds = inds.size();

  ListOf<ListOf<NumericMatrix> > z_grad = clone(z_grad_in);
//for (int t = lag;t < Nt;t++) {
for (int indst = 0; indst < Ninds;indst++) {
    int t = inds(indst);
    double temp = 0;
    for (int i = 0;i < p;i++) {
      for (int k = 0;k < lag;k++) {
        temp += Z[i][k](X(t,q - 1)-1,X(t-k - 1,i)-1);
      }
    }
    temp += Z[p][0](X(t,q - 1)-1,0);
    double grad_t = 1/(temp + eps);
    for (int i = 0;i < p;i++) {
      for (int k = 0;k < lag;k++) {
        z_grad[i][k](X(t,q-1)-1,X(t-k-1,i)-1) -= grad_t;
      }
    }
    z_grad[p][0](X(t,q-1)-1,0) -= grad_t;
  }
    if (pen_type == "ridge") {
    for (int i = 0; i < p;i++) {
      for (int k = 0;k <p;k++) {
        //z_grad[i][k] += mult_mat(Z[i][k],lambda);
        z_grad[i][k] += lambda*Z[i][k];
      }
    }
  }
  if (pen_type == "l1") {
    for (int i = 0;i < p; i++) {
      for (int k = 0;k < lag;k++) {
        int n = Z[i][k].ncol();
        int m = Z[i][k].nrow();
        double nd = (double) n;
        for (int j = 0;j < m;j++) {
          for (int b = 0;b < n; b++) {
             z_grad[i][k](j,b) += (lambda/nd);
          }
        }
      }
    }
  }
  double eps2=.0000000000001;
  if (pen_type == "group_lasso") {
    for (int i = 0;i < p; i++) {
      for (int k = 0;k<lag;k++) {
        int n = Z[i][k].ncol();
        int m = Z[i][k].nrow();
        double norm = fnorm(Z[i][k]);
        if (norm != 0) {
            //for (int j = 0;j < m;j++) {
            //    for (int b = 0;b < n;b++) {
           //         if (z_grad[i][k](j,b) != 0) {
           //             z_grad[i][k](j,b) += lambda*(exp(log(Z[i][k](j,b)) - log(norm)));
           //         }
           //     }
           // }
          z_grad[i][k] += lambda*(Z[i][k]/norm);
        } else {
            for (int j = 0;j < m;j++) {
                for (int b = 0;b < n;b++) {
                    z_grad[i][k](j,b) += lambda;
                }
            }
        }
        //z_grad[i][k] += mult_mat(Z[i][k],lambda/norm);
      }
    }
  }
  return(z_grad);
}



