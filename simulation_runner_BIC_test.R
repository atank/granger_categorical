#setwd('/Users/alextank/Documents/granger_categorical')
source('gen_dat_cat.R')
#library("doParallel")
library("Matrix")


cross_validations_wlam <- function(N,d_gen,meth,m,pen,seed,lam,nseries) {
  pen_types = c("l1","group_lasso"); pen_type = pen_types[pen]
  methods = c("glm","mtd"); method = methods[meth]
  data_gens = c("glm","mtd","latent_var"); data_gen = data_gens[d_gen]
  basefilename = paste('results/',seed,N,method,data_gen,m,lam,pen_type,nseries,'.RData',sep="")
  basefilename_gamm = paste('results/','gamma',seed,N,method,data_gen,m,lam,pen_type,nseries,'.Rdata',sep="")
  set.seed(100)
  p=nseries
  lag = 1
  hold_out_m = 4
  #ncv=2
  series_on = matrix(1,p,lag)
  
  #outd = floor(N/ncv)
  #ind_list = list()
  inds = (lag + 1):(hold_out_m*N)
  test_inds = (N+1):(hold_out_m*N)
  
  #allin = c()
  #for (i in 1:ncv) {
  #  ind_list[[i]] = sample(setdiff(inds,allin),min(outd,length(setdiff(inds,allin))),replace=FALSE)
  #  allin = c(allin,ind_list[[i]])
  #}
  
  graph_final = simulation_runner(seed,p,hold_out_m*N,meth,d_gen,lam,pen,m,test_inds,basefilename_gamm) 
  print(basefilename)
  return(list(graph_final,basefilename_gamm))
  #aucbic = list()
  #for (j in 1:ncv) {
  #   print(j)
  #   aucbic[[j]] = simulation_runner(seed,p,N,meth,d_gen,lam,pen,m,ind_list[[j]])
  #}
  #lam_full = lam*length(inds)/length(setdiff(inds,ind_list[[1]]))
  #aucbic_full = simulation_runner(seed,p,N,meth,d_gen,lam_full,pen,m,c())
  #aucbic_avg = aucbic[[1]]
  #for (i in 2:ncv) {
  #  aucbic_avg = aucbic_avg + aucbic[[i]]
  #}
  #aucbic_out = aucbic_full
  #aucbic_avg = aucbic_avg/ncv
  #aucbic_out[6] = aucbic_avg[6]
  #aucbic_avg = aucbic_out
  #save(aucbic_avg,file=basefilename)
}


####sdfsdf####
cross_validations <- function(N,d_gen,meth,m,pen,seed) {
  N = 200
  d_gen = 2
  meth = 2
  m = 3
  pen = 1
  seed = 2
  
  
  
  
  registerDoParallel(cores=8)
  pen_types = c("l1","group_lasso"); pen_type = pen_types[pen]
  methods = c("glm","mtd"); method = methods[meth]
  data_gens = c("glm","mtd","latent_var"); data_gen = data_gens[d_gen]
  basefilename = paste('results/',seed,N,method,data_gen,m,'.RData',sep="")
  
  #lam_base = c(1,5,10,15,20,25,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,300,400,500)
  lam_base = seq(1,5,by=1)
  aucbic = matrix(0,2,length(lam_base))
  ncv = 5
  p=15
  lag = 1
  
  series_on = matrix(1,p,lag)
  
  outd = floor(N/ncv)
  ind_list = list()
  inds = (lag + 1):N
  allin = c()
  #for (i in 1:ncv) {
  #  ind_list[[i]] = sample(setdiff(inds,allin),min(outd,length(setdiff(inds,allin))),replace=FALSE)
  #  allin = c(allin,ind_list[[i]])
  #}
  #print(ind_list)
  
  aucbic <- foreach(j = 1:ncv) %:%
    foreach(i = 1:length(lam_base),.combine='cbind') %do% {
      print(c(seed,p,N,meth,d_gen,lam_base[i],pen,m))
      simulation_runner(seed,p,N,meth,d_gen,lam_base[i],pen,m,ind_list[[j]])
    }
  
  
  aucbic_avg = aucbic[[1]]
  for (i in 2:ncv) {
    aucbic_avg = aucbic_avg + aucbic[[i]]
  }
  aucbic_avg = aucbic_avg/ncv
  
  bic_s = ((aucbic_avg[2,] - min(aucbic_avg[2,]))/max(aucbic_avg[2,] - min(aucbic_avg[2,]))/2) + .5
  aic_s = ((aucbic_avg[4,] - min(aucbic_avg[4,]))/max(aucbic_avg[4,] - min(aucbic_avg[4,]))/2) + .5
  logp_s = ((aucbic_avg[5,] - min(aucbic_avg[5,]))/max(aucbic_avg[5,] - min(aucbic_avg[5,]))/2) + .5
  cv_s = ((aucbic_avg[6,] - min(aucbic_avg[6,]))/max(aucbic_avg[6,] - min(aucbic_avg[6,]))/2) + .5
  
  #save(aucbic_avg,file=basefilename)
  print(basefilename)
  #plot(lam_base,bic_s,type="l",col="red",xlab = "lambda value",ylab = "bic/auc")
  #lines(lam_base,aucbic_avg[1,],col="blue")
  #lines(lam_base,aic_s,col="green")
  #lines(lam_base,logp_s,col="purple")
  #lines(lam_base,cv_s,col="yellow")
}







lambda_grid <- function() {
  registerDoParallel(cores=8)
  
  #lam_base = c(1,5,10,15,20,25,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,300,400,500)
  lam_base = seq(1,100,by=3)
  aucbic = matrix(0,2,length(lam_base))
  N = 200
  ncv = 5
  outd = floor(N/ncv)
  ind_list = list()
  lag = 1
  inds = (lag + 1):200 - 1 #-1 because they are going into c++
  allin = c()
  for (i in 1:ncv) {
    ind_list[[i]] = sample(setdiff(inds,allin),min(outd,length(setdiff(inds,allin))),replace=FALSE)
    allin = c(allin,ind_list[[i]])
  }
  
  aucbic <- foreach(i = 1:length(lam_base),.combine=cbind) %do% {
    print(i)
    print("THE LAMDA IS")
    simulation_runner(4238674,15,200,2,2,lam_base[i],1,3,inds)
  }
  
  bic_s = ((aucbic[2,] - min(aucbic[2,]))/max(aucbic[2,] - min(aucbic[2,]))/2) + .5
  aic_s = ((aucbic[4,] - min(aucbic[4,]))/max(aucbic[4,] - min(aucbic[4,]))/2) + .5
  logp_s = ((aucbic[5,] - min(aucbic[5,]))/max(aucbic[5,] - min(aucbic[5,]))/2) + .5
  
  
  plot(lam_base,bic_s,type="l",col="red",xlab = "lambda value",ylab = "bic/auc")
  lines(lam_base,aucbic[1,],col="blue")
  lines(lam_base,aic_s,col="green")
  lines(lam_base,logp_s,col="purple")
  
  
  
}






simulation_runner <-function(seed,p,N,meth,d_gen,lambda_base,pen,m,inds,basefilename_gamm) {
  #print("EHEREHRE")
  #seed = 32322323
  #p = 15
  #N = 500
  #meth = 2
  #d_gen = 2
  #lambda_base = .1
  #m = 3
  #pen = 2;
  inds = inds - 1
  lag = 1
  indset = setdiff(((lag + 1):N) - 1,inds)
  
  Nc = rep(m,p)
  #lag = 1
  
  thresh = .0001
  spar_bern = .2
  spar_gam = 10; spar_p_table = 1; total_variation_thresh = .2
  lambda = lambda_base
  pen_types = c("l1","group_lasso"); pen_type = pen_types[pen]
  methods = c("glm","mtd"); method = methods[meth]
  proj_type = "quadprog"
  data_gens = c("glm","mtd","latent_var"); data_gen = data_gens[d_gen]
  set.seed(seed*2032)
  series_on = matrix(1,p,lag)
  basefilename = paste("results/",seed,p,N,meth,d_gen,lambda_base,pen,sep="_")
  gammaest = matrix(0,p,p)
  model_size = rep(0,p)
  logfinal = rep(0,p)
  logfinal_cv = rep(0,p)
  granger_mat_est = matrix(0,p,p)
  z_estimate = list()
  
  if (data_gen == "glm") {
    total_variation_thresh = .1
    out = generate_sparse_GLM_granger(Nc,total_variation_thresh,spar_bern,lag)
    Ztrue = out[[1]]; granger_mat = out[[2]]
    X = generate_cat_dat_GLM(Ztrue,N,0,lag,p,Nc)
  }
  if (data_gen == "mtd") {
    out = generate_sparse_MTD(spar_gam,spar_p_table,p,m,lag,spar_bern,total_variation_thresh)
    p_table = out[[1]]; gamma = out[[2]]; z_table = out[[3]];granger_mat = (gamma > 0)*1
    X = generate_cat_dat(p_table,gamma,N,0,lag,m)
    print(X)
  }
  if (data_gen == "latent_var") {
    out = generate_sparse_VAR(spar_bern,p)
    A = out[[1]];granger_mat = out[[2]];
    X = generate_cat_var(A,N,p,m,lag)
  }
  if (method == "glm") {
    source('GLM_granger.R')
  }
  if (method == "mtd") {
    source('PG_MTD.R')
  }
  
  for (q in 1:1) {
    if (data_gen == "glm") {
      #p_tensor_q = compute_probability_tensor_GLM(Ztrue[[q]],lag,Nc,q)
    }
    if (data_gen == "mtd") {
      #p_tensor_q = compute_probability_tensor(array(as.matrix(z_table[,,,,q]),c(m,m,p,lag)),lag)
    }
    
    
    if (method == "glm") {
      Ze = init_Z_list(p,lag,Nc,q)
      out = proximal_GLM_granger(X,q,lag,Nc,lambda,Ze,pen_type,matrix(1,p,lag),indset)
      z_estimate = out[[1]]
      logp = out[[2]]
      logfinal[q] = loglikelihood_cpp(z_estimate,X,lag,Nc,q,indset)
      logfinal_cv[q] = loglikelihood_cpp(z_estimate,X,lag,Nc,q,inds)
    }
    if (method == "mtd") {
      indset_temp = inds
      inds_temp = indset
      inds = inds_temp
      indset = indset_temp
      
      
      out = PG_MTD(X,lambda,p,lag,Nc,pen_type,proj_type,q,indset)
      z_estimate = out[[1]]
      logp = out[[2]]
      logfinal[q] = log_probability_MTD_cpp(X,z_estimate,q,lag,p,0,pen_type,indset)
      logfinal_cv[q] = log_probability_MTD_cpp(X,z_estimate,q,lag,p,0,pen_type,inds)
    }
    out = compute_sparsity(z_estimate,p,lag,thresh);gammaest[q,] = out[[2]];
    granger_mat_est[q,] = out[[1]]
  }
  out = compute_roc(granger_mat,gammaest)
  AUC = out[[2]];fptp = out[[1]]
  out = BIC_score(logfinal,granger_mat_est,Nc,length(indset),method);BIC = out[[1]];msize=out[[2]];AIC = out[[3]]
  graph_final = list(); graph_final$gammaest = gammaest;graph_final$granger_mat = granger_mat;
  #if (length(inds) == 0) {
  #write.csv(fptp,paste(basefilename,'fptp','.csv',sep=""))
  #write.csv(c(AUC,BIC,AIC,sum(logfinal_cv)),paste(basefilename,"aucbic",".csv",sep=""))
  graph_final$aucbic = c(AUC,BIC,msize,AIC,sum(logfinal),sum(logfinal_cv))
  #save(graph_final,file = basefilename_gamm)
  #write.csv(granger_mat,paste(basefilename,"graph_true",'.csv',sep=""))
  # }
  return(graph_final)
}


BIC_score <- function(logfinal,gmat_est,Nc,n,model) {
  p = length(Nc)
  msize = 0
  if (model == "mtd") {
    msize = 0
    for (i in 1:1) {
      msize = msize + Nc[i]  - 1
      for (j in 1:p) {
        msize = msize + ((Nc[i] - 1)*(Nc[j]) - Nc[i])*gmat_est[i,j]
      }
    }
    msize = msize + sum(gmat_est)
  }
  #if (model == "mtd") {
  # msize = msize + sum(gmat_est)
  #}
  #msize = msize + sum(Nc  - 1)
  return(list(2*sum(logfinal) + msize*log(n),msize,2*sum(logfinal) + 2*msize))
}

#BIC_score <- function(logfinal,gmat_est,Nc,n,model) {
#  p = length(Nc)
#  msize = 0
#  for (i in 1:p) {
#    for (j in 1:p) {
#      msize = msize + (Nc[i] - 1)*(Nc[j] - 1)*gmat_est[i,j]
#    }
#  }
#  if (model == "mtd") {
#    msize = msize + sum(gmat_est)
#  }
#  msize = msize + sum(Nc  - 1)
#  return(list(2*sum(logfinal) + msize*log(n-1),msize,2*sum(logfinal) + 2*msize))
#}



#task_id <- as.numeric(Sys.getenv('SLURM_ARRAY_TASK_ID'))
#task_id = 10
#args <- (commandArgs(TRUE))
#lambda = task_id
MTD_lam_top = 200
GLM_lam_top = 25
total_lam = 300
lambda_MTD = seq(0,MTD_lam_top,by=MTD_lam_top/total_lam)
#lambda_GLM = seq(0,GLM_lam_top,by=GLM_lam_top/total_lam)

#if(length(args)==0){
#  print("No arguments supplied.")
  N = 200
  d_gen = 3
  meth_base = 2
  m = 3
  seed = 8
  task_id = 58
  nseries = 25
#} else {
#  for (i in 1:length(args)) {
#    eval(parse(text=args[[i]]))
#  }
#}

if (meth_base == 3) {
  meth = 2
  pen = 2
}
if (meth_base == 2) {
  meth = 2
  pen = 1
}
if (meth_base == 1) {
  meth = 1
  pen = 2
}

if (meth == 2) {
  lambda = lambda_MTD[task_id]
}
if (meth == 1) {
  lambda = lambda_GLM[task_id]
}
graph_final = list()
basefilename = list()
for (i in 1:length(lambda_MTD)) {
  print(i)
  #print("SEED")
  #print("seed")
  #seedi=7
  out = cross_validations_wlam(N,d_gen,meth,m,pen,7,lambda_MTD[i],nseries)
  graph_final[[i]] = out[[1]] 
  #basefilename[[seedi]] = out[[2]]
}
#print(basefilename[[1]])
#save(graph_final,file=basefilename[[1]])
#cross_validations_wlam(200,2,1,3,2,4,3)
loglik_scores = rep(0,length(lambda_MTD))
bic_scores = rep(0,length(lambda_MTD))
aic_scores = rep(0,length(lambda_MTD))
msize = rep(0,length(lambda_MTD))
loglik_scores_ac = rep(0,length(lambda_MTD))

for (i in 1:length(lambda_MTD)) {
  loglik_scores[i] = graph_final[[i]]$aucbic[6]
  bic_scores[i] = graph_final[[i]]$aucbic[2]
  aic_scores[i] = graph_final[[i]]$aucbic[4]
  msize[i] = graph_final[[i]]$aucbic[3]
  loglik_scores_ac[i] = graph_final[[i]]$aucbic[5]
}
plot(loglik_scores)
plot(bic_scores)
plot(msize)
plot(aic_scores)
plot(2*loglik_scores_ac + log(N)*msize)

plot(2*loglik_scores_ac)
lines(log(N)*msize - min(log(N)*msize) + min(2*loglik_scores_ac))


