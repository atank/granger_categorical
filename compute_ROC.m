function [tp_rate,fp_rate] = compute_ROC(ze,gammat,gammainf,pen_type,p,m)
thresh = .000001;
if (strcmp(pen_type,'nuc') || strcmp(pen_type,'group_mean_fused'))
    gammainf = 0*gammainf;
    for i = 1:p
        for j = 1:p
            mf = mean(ze(:,:,j,i),2);
            gammainf(i,j) = sqrt(sum(sum((ze(:,:,j,i) - repmat(mf,1,m)).^2)));
        end
    end 
end
tp = gammat > thresh;
ep = gammainf > thresh;
tn = gammat < thresh;
en = gammainf < thresh;
FP = sum(sum(ep.*tn));
TN = sum(sum(tn.*en));
TP = sum(sum(tp.*ep));
FN = sum(sum(en.*tp));
fp_rate = FP/(FP + TN);
tp_rate = TP/(TP + FN);
%disp(FN);
%disp(fp_rate);
end
