import os

from itertools import product

jfile = "bach_cv.sh"

ncv = 5
meth = [2]
pens = [2]
cvs = range(1,ncv+2,1)
#pen = 2

basejfile = "bach_cat_slurm.sbatch"
par_prod = product(cvs,meth,pens)
for par in par_prod:
    cv,me,pen = par
    if (pen == 1 and  me == 1):
        continue
    #print(pen)
    #print(me)
    jfile_b = "bach_cv_%d_me_%d_pen_%d" % (cv,me,pen)
    jfile = "sbatch_files_bach/" + jfile_b + basejfile
    with open(jfile,"w") as f:
        f.write("#!/bin/bash" + "\n")
        f.write("\n")
        f.write("#BATCH --job-name tankjob_bach" + "\n")
        f.write("#SBATCH --partition medium" + "\n")
        f.write("#SBATCH --ntasks 1" + "\n")
        f.write("#SBATCH --time 1-00:00" + "\n")
        f.write("#SBATCH --mem-per-cpu=4000M" + "\n")
        f.write("#SBATCH -o outs_bach/" + jfile_b + "gran_bach_%j.out" + "\n")
        f.write("#SBATCH -e outs_bach/" + jfile_b + "gran_bach_%j.out" + "\n")
        f.write("#SBATCH --mail-user=alextank@uw.edu" + "\n")
        f.write("\n")
        args = "'--args cv=%d ncv=%d meth=%d pen=%d'" % (cv,ncv,me,pen)
        temp = "%d_%d_%d_%d" % (cv,ncv,me,pen)
        f.write("R CMD BATCH " + args + " data_analyzer.R " "outs_bach/" + temp + '_'
        + '"${SLURM_ARRAY_TASK_ID}".Rout')
        os.chmod(jfile,0755)

jfile_r = "master_bach_grang_slurm.sh"

par_prod = product(cvs,meth,pens)
with open(jfile_r,'w') as f:
    f.write("#!/bin/bash" + "\n")
    f.write("\n")
    for par in par_prod:
        cv,me,pen = par
        if (pen == 1 and me == 1):
            continue
        jfile_b = "sbatch_files_bach/bach_cv_%d_me_%d_pen_%d" % (cv,me,pen)
        jfile = jfile_b + basejfile
        f.write("sbatch --array=1-1000 " + jfile + "\n")
os.chmod(jfile_r,0755)
