#setwd('/Users/alextank/Documents/granger_categorical')
source('gen_dat_cat.R')
#library("doParallel")
library("Matrix")



create_clean_bach_data <- function(top_b,top_c) {
top_b = 200
top_c = 200
X = read.csv("jsbach_chorals_harmony.csv")
Nt = dim(X)[1]
songs = unique(X[,1])
Ns = length(songs)
bass_pitch = unique(X[,15])
Nbass = length(bass_pitch)
chord = unique(X[,17])
nchord = length(chord)
meter = unique(X[,16])
nmeter = length(meter)
start_inds = c(1,which(X[,2] == 1))
include_inds = setdiff(1:Nt,start_inds) 

chord_counts = rep(0,nchord)
bass_pitch_counts = rep(0,Nbass)
for (i in 1:Nt) {
  chord_counts[which(chord == X[i,17])] = chord_counts[which(chord == X[i,17])] + 1
  bass_pitch_counts[which(bass_pitch == X[i,15])] = bass_pitch_counts[which(bass_pitch == X[i,15])] + 1
  
}

keep_bass = bass_pitch[which(bass_pitch_counts > top_b)]
keep_chord = chord[which(chord_counts > top_c)]

yesno = unique(X[,4])
Y = matrix(0,Nt,15)

Nc = c(rep(2,12),length(keep_bass) + 1,nmeter,length(keep_chord)+1)

###create cleaned data set###
for (i in 1:Nt) {
  for (j in 3:14) {
    if (X[i,j] == yesno[1]) {
      Y[i,j - 2] = 1
    }
    else {
      Y[i,j - 2] = 2
    }
  }
  if (X[i,17] %in% keep_chord) {
    ind = which(keep_chord == X[i,17])
    Y[i,15] = ind
  } 
  else {
    Y[i,15] = length(keep_chord) + 1
  }
  if (X[i,15] %in% keep_bass) {
    ind = which(keep_bass == X[i,15])
    Y[i,13] = ind
  } 
  else {
    Y[i,13] = length(keep_bass) + 1
  }
  Y[i,14] = X[i,16]
}
return(list(Y,Nc,start_inds))

}

fit_graph <- function(pen_type,meth,lam,X,Nc,indset,inds) {
  p = length(Nc)
  thresh = .00001
  logfinal = rep(0,p)
  logfinal_cv = rep(0,p)
  gammaest = matrix(0,p,p)
  granger_mat_est = matrix(0,p,p)
  z_estimate_l = list()
  for (q in 1:p) {
    print(q)
    if (meth == "glm") {
      Ze = init_Z_list(p,lag,Nc,q)
      out = proximal_GLM_granger(X,q,lag,Nc,lam,Ze,pen_type,matrix(1,p,lag),indset)
      z_estimate = out[[1]]; logp = out[[2]]
      logfinal[q] = loglikelihood_cpp(z_estimate,X,lag,Nc,q,indset)
      if (length(inds) > 0) {
        logfinal_cv[q] = loglikelihood_cpp(z_estimate,X,lag,Nc,q,inds)
      }
    }
    if (meth == "mtd") {
      proj_type = "quadprog"
      out = PG_MTD(X,lam,p,lag,Nc,pen_type,proj_type,q,indset)
      z_estimate = out[[1]]
      logp = out[[2]]
      logfinal[q] = log_probability_MTD_cpp(X,z_estimate,q,lag,p,0,pen_type,indset)
      if (length(inds) > 0) {
        logfinal_cv[q] = log_probability_MTD_cpp(X,z_estimate,q,lag,p,0,pen_type,inds)
      }
      z_estimate_l[[q]] = z_estimate
    }
    #out = compute_sparsity(z_estimate,p,lag,thresh);gammaest[q,] = out[[2]];
    out = compute_sparsity_norm(z_estimate,p,lag,thresh,meth);gammaest[q,] = out[[2]];
    granger_mat_est[q,] = out[[1]]
  }
  out = BIC_score(logfinal,granger_mat_est,Nc,N-lag,meth);BIC = out[[1]];msize=out[[2]];AIC = out[[3]]
  return(list(c(BIC,msize,AIC,sum(logfinal),sum(logfinal_cv)),gammaest,z_estimate_l))
}

cross_validate <- function(X,inds,ncv,meth,pen,lam,cv,task_id) {
  pen_types = c("l1","group_lasso"); pen_type = pen_types[pen]
  methods = c("glm","mtd"); method = methods[meth]
  #basefilename = paste('results_bach/',method,task_id,cv,pen_type,'.RData',sep="_")
  basefilename = paste('results_finance/',method,task_id,cv,pen_type,'.RData',sep="_")
  outd = floor(N/ncv)
  ind_list = list()
  allin = c()
  set.seed(100)
  for (i in 1:ncv) {
    ind_list[[i]] = sample(setdiff(inds,allin),min(outd,length(setdiff(inds,allin))),replace=FALSE)
    allin = c(allin,ind_list[[i]])
  }
  ind_list[[ncv+1]] = c(3)
  aucbic = fit_graph(pen_type,method,lam,X,Nc,setdiff(inds,ind_list[[cv]]),ind_list[[cv]])
  save(aucbic,file=basefilename)
}

task_id <- as.numeric(Sys.getenv('SLURM_ARRAY_TASK_ID'))
args <- (commandArgs(TRUE))
lambda = task_id
for (i in 1:length(args)) {
    eval(parse(text=args[[i]]))
}


lams = exp(seq(-10,log(25),by = ((log(25)-(-10))/1000)))
lambda = lams[task_id]
#meth = 2
#lambda = 50
#cv = 1
#ncv = 5

print(meth)
print(ncv)
print(cv)
print(lambda)

if (meth == 1) {
  source("GLM_granger.R")
}
if (meth == 2) {
  source('PG_MTD.R')
}

out = create_clean_bach_data(100,100)
X = out[[1]];Nc = out[[2]];start_inds = out[[3]]
p = length(Nc)
N = dim(X)[1]
lag = 1
indset = setdiff(1:N,start_inds) - 1
#ncv = 2
cross_validate(X,indset,ncv,meth,pen,lambda,cv,task_id)
  
