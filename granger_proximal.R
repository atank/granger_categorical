####this function has the proximal operators used####
group_soft_threshold <- function(x,lam) {
  temp = (1 - lam/norm(as.matrix(x),'f'))
  if (temp > 0) {return(temp*x)}
  if (temp <= 0) {return(0*x)}
}

gcgfl <- function(Z,lam,onezm) {
  thresh = .00001
  diff = 100
  z = apply(Z,1,mean)
  beta = group_soft_threshold(Z - z %*% t(onezm),lam)
  while (diff > thresh) {
    beta_old = beta
    z_old = z
    z = apply(Z - beta,1,mean)
    beta = group_soft_threshold(Z - z %*% t(onezm),lam)
    diff = max(max(abs(z-z_old)),max(abs(beta_old - beta)))
  }
  return(beta + z %*% t(onezm))
}