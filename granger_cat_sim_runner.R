####code to generate data and run optimization scripts####
setwd('/Users/alextank/Documents/granger_categorical')
source('gen_dat_cat.R')
source('MTD_proximal_gradient.R')

###simulation parameters####
#set.seed(3232477)
set.seed(32477) #death seed

p = 2
lag = 1
Ttrain = 300
Ttest = 10
m = 2
out = create_equal_column_projection_matrix(m,m)
Q = out[[1]]
spar_gam = 10
spar_p_table = .7
spar_bern = 1

###estimation parameters####
linesearch = TRUE
lambda = 0
pentype = "group_lasso"
accelerated = FALSE
q = 1

out = generate_random_MTD(spar_gam,spar_p_table,p,m,lag)
out = generate_sparse_MTD(spar_gam,spar_p_table,p,m,lag,spar_bern)
###generate data###
p_table = out[[1]]
gamma = out[[2]]
z_table = out[[3]]
qdim = array(as.matrix(z_table[,,,,q]),c(m,m,p,lag))
p_tensor_q = compute_probability_tensor(array(as.matrix(z_table[,,,,q]),c(m,m,p,lag)),lag)
#p_tensor_q_p3 = compute_probability_tensor_p3(array(as.matrix(z_table[,,,,q]),c(m,m,p,lag)),lag)
X = generate_cat_dat(p_table,gamma,Ttrain,Ttest,lag)

###initial_estimates###
###z_table = compute_z_table(gamma,p_table)
z_table_init_np = abs(array(rnorm(p*lag*m*m),dim(qdim)))
z_table_init = MTD_proximal2(z_table_init_np,Q,p,lag)
z_table_init[,,,] = 1/m 
####begin optimization####
out = proximal_gradient_MTD(X,p,lag,m,q,z_table_init,lambda,linesearch,pentype,accelerated)
z_estimate = out[[1]]
logp = out[[2]]
plot(logp,type="l")
p_test_q_est = compute_probability_tensor(z_estimate,lag)

apply(z_estimate[,,1,1],2,sum)

sum(z_estimate[,1,,])
p_test_q_est
p_tensor_q

p_test_q_est - p_tensor_q

######with penalty####
