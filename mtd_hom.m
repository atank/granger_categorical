rng(183190)
p = 6;
m = 2;
T = 500;
dim = m*ones(1,p+1);
X = zeros(T,1);
onzm = ones(m,1);
onzp = ones(p,1);
onzpm = ones(p*m,1);

zt = zeros(m,m,p);
temp = drchrnd(2*ones(m,1)',m);
for i = 1:p
    %temp = drchrnd(2*ones(m,1)',m);
    zt(:,:,i) = temp';
end
gammat = [.5 .2 .1 .1 .05 .05];

%for (i in 1:

%gammat(1,3) = .0001;
%gammat(1,:) = gammat(1,:)/sum(gammat(1,:));
X(1:p) = 1;
for t = (p+1):T
    temp = zeros(m,1);
    for i = 1:p
        temp = temp + gammat(i)*zt(:,X(t-i),i);
    end
    disp(temp);
    disp([X(t-1) X(t-2) X(t-2)]);
    X(t) = randsample(m,1,true,temp);
end

lam = 30;

cvx_begin
    variable z(m,m,p) nonnegative;
    %variable gammae(p) nonnegative;
    expression x(T-p)
    for t = (p + 1):(T)
        temp = 0;
        for i = 1:p
            temp = temp + z(X(t),X(t-i),i);
        end
        x(t-p) = -log(temp);
    end
    
    %expression pen(p)
    %for i = 1:p
        %pen(i) = norm(z(:,m+1,i),1.2);
        %pen(i) = norm(vec(z(:,:,i)));
    %end
    expression ZV(m*m,p)
    for i = 1:p
        ZV(:,i) = vec(z(:,:,i));
    end
    
    %%%no sparsity
    %minimize(sum(x))
    %minimize(sum(x) + lam*sum(pen))
    minimize(sum(x) + lam*norm_nuc(ZV))
    subject to
        %gammae >= .0001;
        %gammae'*onzp == 1;
        onzpm'*vec(squeeze(z(:,1,:))) == 1;
        %gamma > 0;
        %z > 0;
        for i = 1:p 
            for j = 2:m
                onzm'*z(:,j,i) - onzm'*z(:,1,i) == 0;
            end
        end
cvx_end
ze = zeros(m,m,p);

gammae = onzm'*squeeze(z(:,1,:));

for i = 1:p
    ze(:,:,i) = z(:,:,i)/gammae(i);
end
