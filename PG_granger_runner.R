setwd('/Users/alextank/Documents/granger_categorical')
source('gen_dat_cat.R')
source('PG_MTD.R')
#source('MTD_ADMM.R')

run_PC_granger <- function() {
set.seed(328833) #death seed
p = 15
lag = 1
Ttrain = 1000
Ttest = 3
m = 3
Nc = rep(m,p)
q = 1
total_variation_thresh = .3;
spar_gam = 10
spar_p_table = 1
spar_bern = .5;
rho = 5

lambda_base = .12
lambda = lambda_base*Ttrain
pentype = "group_lasso"
pentype = "l1"

#pentype = "ridge"
#pentype = "gcgfl"
#pentype = "nuc"

proj_type = "quadprog"
q = 1

out = generate_sparse_MTD(spar_gam,spar_p_table,p,m,lag,spar_bern,total_variation_thresh)
p_table = out[[1]]
gamma = out[[2]]
z_table = out[[3]]
z_table_con = out[[4]]
#p_tensor_q = compute_probability_tensor(array(as.matrix(z_table[,,,,q]),c(m,m,p,lag)),lag)
X = generate_cat_dat(p_table,gamma,Ttrain,Ttest,lag,m)
out = PG_MTD(X,lambda,p,lag,Nc,pentype,proj_type,q)
z_estimate = out[[1]]
logp = out[[2]]
plot(logp,type="l",col="blue")

print(z_estimate)
print(z_table_con[[q]])


#out = MTD_ADMM(X,q,rho,p,lag,Nc,lambda,pentype,rep(1,p))
#z_estimateAD = out[[1]]
#logp = out[[2]]
#plot(logp,type="l")

# apply(z_estimate[[1]][[1]],2,sum)[1] + apply(z_estimate[[2]][[1]],2,sum)[1] + apply(z_estimate[[3]][[1]],2,sum)[1]
# 
# ##apply(z_estimate[,,2,1],2,sum)
# ##sum(z_estimate[,1,,])
# 
gammaest = compute_sparsity(z_estimate,p,lag,.00001)
# 
print((gamma[q,] > .0000001)*1)
print((gammaest[[2]] > .0001)*1)
# gammaest
# 
#p_test_q_est = compute_probability_tensor_MTD(z_estimate,lag,Nc,q)
#counts = calculate_count_tensor(t(X),q,rep(m,p),lag)

# #p_test_q_est - counts[[1]]
# 
# #norm(as.matrix(p_test_q_est - counts[[1]]))
# 
# norm(as.matrix(p_test_q_est - p_tensor_q))


# p = 50
# Nc = rep(4,p)
# q = 1
# Z = list()
# for (i in 1:p) {
#   Z[[i]] = list()
#   for (k in 1:lag)
#     Z[[i]][[k]] = matrix(rnorm(Nc[q]*Nc[i],0,1),Nc[q],Nc[i])
# }
# Z[[p+1]] = list();Z[[p+1]][[1]] = matrix(0,Nc[q],Nc[q]);Z[[p+1]][[1]][,1] = rnorm(Nc[q],0,1)
# step_size = 0
# gradZ = Z
# proj_param = compute_projection_MTD_ADMM_dc(lag,Nc,p,q)
# proj_type = "quadprog"
# 
# 
# MTD_projection(gradZ,Z,step_size,p,lag,proj_param,proj_type)
  

}