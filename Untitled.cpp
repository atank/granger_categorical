//#include <Rcpp.h>
#include <RcppArmadillo.h>
using namespace Rcpp;
using namespace arma;
// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

// [[Rcpp::depends("RcppArmadillo")]]

void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}
void showValue_int(int x) {
    Rcout << "The value is " << x << std::endl;
}
void showMatrix(NumericMatrix X) {
    Rcout << "matrix is" << std::endl << X << std::endl;
}


// [[Rcpp::export]]
int timesTwo(int x) {
   return x * 2;
}

// [[Rcpp::export]]
List test() {
  std::vector< arma::cube > v;
  arma::cube x1(2,2,2);
  x1.zeros();
  x1(1,1,0) = 3;
  arma::cube x2(2,2,2);
  v.push_back( x1 );
  return wrap( v );
}

List soft_weights_cpp(IntegerMatrix X,ListOf<ListOf<NumericMatrix> > Z,int p,int lag,int q,ListOf<ListOf<NumericMatrix> > P,IntegerVector Nc) {
  int Nt = X.nrow();
  ListOf<ListOf<NumericMatrix> > Ps = clone(P);

  for (int t = lag;t < Nt; t++) {
    NumericMatrix psum = NumericMatrix(p,lag);
    
    
    double total = 0;
    int refq = X(t,q)-1;
    for (int i = 0;i < p;i++) {
      for (int k = 0; k < lag; k++) {
        //int refq = X(t,q)-1;
        int reftk = X(t-k-1,i)-1;
        psum(i,k) = Z[i][k](refq,reftk);
        total += Z[i][k](refq,reftk);
      }
    }
    double p_int = Z[p][0](refq,0);
    total += p_int;
    for (int i = 0; i< p;i ++) {
      for (int k = 0;k < lag;k++) {
        psum(i,k) /= total;
      }
    }
    p_int /= total;
    for (int i = 0; i < p;i++) {
      for (int k = 0;k < lag;k++) {
        //int refq = X(t,q)-1;
        int reftk = X(t-k-1,i)-1;
        Ps[i][k](refq,reftk) += psum(i,k);
      }
    }
    Ps[p][0](refq,0) += p_int;
  }
  
  return(Ps);
}

double Z_difference_cpp(ListOf<ListOf<NumericMatrix> > Z_old,ListOf<ListOf<NumericMatrix> >Z ,IntegerVector Nc,int p,int lag,int q) {
  double diff = 0;
  for (int i = 0;i < p;i++) {
    for (int k = 0;k < lag;k++) {
      for (int j = 0;j < Nc[q];j++) {
        for (int l = 0;l < Nc[i];l++) {
          diff += pow(Z_old[i][k](j,l) - Z[i][k](j,l),2.0);
        }
      }
    }
  }
  for (int j = 0;j < Nc[q];j++) {
    diff += pow(Z_old[p][0](j,0) - Z[p][0](j,0),2.0);
  }
  //showValue(diff);
  return(pow(diff,.5));
}



// [[Rcpp::export]]
List MTD_MM_cpp(IntegerMatrix X, ListOf<ListOf<NumericMatrix> > ZU, ListOf<ListOf<NumericMatrix> > Zin,int lag, IntegerVector Nc, int p, double rho, int q, ListOf<ListOf<NumericMatrix> > Ps) {
  double diff = 100;
  double eps = .0001;
  int iter = 0;
  ListOf<ListOf<NumericMatrix> > Z_old = clone(Zin);
  ListOf<ListOf<NumericMatrix> > Z = clone(Zin);
  int MAX_ITER = 10; 
  while(diff > eps && iter < MAX_ITER) {
  //while(iter < 3) {
    iter++;
    Z_old = clone(Z);
    ListOf<ListOf<NumericMatrix> > P = soft_weights_cpp(X,Z,p,lag,q,Ps,Nc);
    //showValue(P[0][0](0,0));
    for (int i = 0;i < p;i++) {
      for (int k = 0;k < lag;k++) {
        for (int j = 0;j < Nc[q];j++) {
          for (int l = 0;l < Nc[i];l++) {
            Z[i][k](j,l) = -rho*ZU[i][k](j,l) + sqrt(rho*rho*ZU[i][k](j,l)*ZU[i][k](j,l) + 4.0*2.0*rho*P[i][k](j,l));
            Z[i][k](j,l) /= 4.0*rho;
            if (Z[i][k](j,l) < 0) {
              Z[i][k](j,l) = 0;
            }
          }
        }
      }
    }
    ///intercept///
    for (int j = 0;j < Nc[q];j++) {
      Z[p][0](j,0) = -rho*ZU[p][0](j,0) + sqrt(rho*rho*ZU[p][0](j,0)*ZU[p][0](j,0) + 4.0*2.0*rho*P[p][0](j,0));
      Z[p][0](j,0) /= 4.0*rho;
      if (Z[p][0](j,0) < 0) {
        Z[p][0](j,0) = 0;
      }
    }
    
    diff = Z_difference_cpp(Z_old,Z,Nc,p,lag,q);
    //showValue(diff);
    //showMatrix(Z_old[0][0]);
    //showMatrix(Z[0][0]);
  }
  return(Z);
}


  
