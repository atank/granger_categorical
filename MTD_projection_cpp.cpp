#include <RcppArmadillo.h>
#include <Rcpp.h>
#include <RcppArmadilloExtensions/sample.h>
using namespace Rcpp;
// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

// [[Rcpp::export]]
void showValue_int(int x) {
   Rcout << "The value is " << x << std::endl;
}

void showValue_double(double x) {
   Rcout << "The value is " << x << std::endl;
}
// [[Rcpp::export]]
void showValue_vec(arma::vec x) {
    Rcout << "The value is " << x << std::endl;
}

// [[Rcpp::export]]
void showValue_uvec(arma::uvec x) {
    Rcout << "The value is " << x << std::endl;
}

// [[Rcpp::export]]
void showValue_mat(arma::mat x) {
    Rcout << "The value is " << x << std::endl;
}

// [[Rcpp::depends(RcppArmadillo)]]

// [[Rcpp::export]]
arma::vec simplex_projection_cpp(arma::vec ym) {
  int z = 1;
  int last_index = ym.n_elem - 1;
  arma::uvec mu_int = arma::sort_index(-ym);
  arma::vec mu = ym.elem(mu_int);
  //showValue_vec(mu);
  bool cond = true;
  int j = mu_int.n_elem - 1;
  double sum_mu = sum(ym);
  //showValue_double(sum_mu);
  while (cond) {
    double jj = 1/(double)j; 
    //showValue_double(sum_mu);
    //showValue_double((mu(j) - (1/((double)(j+1)))*(sum_mu - (double)z)));
    if ((mu(j) - (1/((double)(j+1)))*(sum_mu - (double)z)) >= 0.0) {
      cond = false;
    } else {
      //showValue_int(j);
      sum_mu -= mu(j);
      j = j - 1;
    }
  }
  //showValue_int(j);
  int rho = j + 1;
  arma::vec temp = 0.0*ym;
  if (rho == 1) {
    //find maximum element
    temp(mu_int(last_index)) = 1.0;
  } else {
    double theta = (1/((double)rho))*(sum_mu - 1.0);
    for (int i = 0; i <= last_index; i++) {
      double temp2 = ym(i) - theta;
      if (temp2 > 0) {
        temp(i) = temp2;
      }
      else {
        temp(i) = 0.0;
      }
    }
  }
  return(temp);
}

// [[Rcpp::export]]
arma::vec project_simplex_full_MTD_cpp(arma::vec z, arma::uvec idx) {
  arma::vec y = z.elem(idx);
  arma::vec y_proj = simplex_projection_cpp(y);
  for (int i = 0;i < y_proj.n_elem; i++) {
    //if (idx(i) == (z.n_elem - 1)) {
    //  showValue_double(y_proj(i));
    //}
    z(idx(i)) = y_proj(i);
  }
  for (int i = 0; i < z.n_elem; i++) {
    if (z(i) < 0) {
      z(i) = 0;
    }
  }
  return(z);
}

// [[Rcpp::export]]
arma::vec project_equal_columns_MTD_cpp(arma::vec z,List eq_proj_mat, arma::uvec start_inds,arma::uvec end_inds,int p) {
  for (int i = 0;i < p;i++) {
    //showValue_int(i);
    SEXP ll = eq_proj_mat[i];
    //showValue_int(1);
    arma::mat eqp = as<arma::mat>(ll);
    //showValue_int(2);
    arma::vec z_temp = z.subvec(start_inds(i), end_inds(i));
    //showValue_int(3);
    //showValue_mat(eqp);
    arma::vec temp = eqp * z_temp;
    //showValue_vec(temp);
    for (int j = start_inds(i); j < (end_inds(i) + 1);j++) {
      //showValue_int(j);
      z(j) = temp(j - start_inds(i));
    }
  }
  //showValue_vec(z);
  return(z);
}

// [[Rcpp::export]]
arma::vec zykstra_projection_cpp(arma::vec x,arma::vec pr,arma::vec y,arma::vec qr,List eq_proj_mat,arma::uvec start_inds,arma::uvec end_inds,int p,arma::uvec inds_simplex,double thresh) {
  //double thresh = .0000000001;
  double diff = 10;
  int iter = 0;
  arma::vec x_past = 0*x;
  int max_iter = 10000;
  while (diff > thresh & iter < max_iter) {
  //while (diff > thresh) { 
  //while (iter < max_iter) {
    iter = iter + 1;
    //showValue_int(iter);
    y = project_simplex_full_MTD_cpp(x + pr,inds_simplex);
    pr = x + pr - y;
    x = project_equal_columns_MTD_cpp(y + qr,eq_proj_mat,start_inds,end_inds,p);
    qr = y + qr - x;
    diff = norm(x - x_past,2);
    //showValue_double(y(y.n_elem-1));
    x_past = 1*x;
  }
  return(y);
}

