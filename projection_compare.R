library("Rcpp")
library("Matrix")
sourceCpp("MTD_projection_cpp.cpp")
source("projection_MTD_funcs.R")
source("PG_MTD.R")

pset = c(10,20,30,40,50,60,70)
#pset = 20
noise = c(.3,.5,.7,1)
noise = .7
reps = 80

#generate random parmeter values#
lag = 1
p = 35
Nc = rep(5,p)
q = 1

# ##generate Z#
# Z = random_initialize_variables_MTD(Nc,p,lag,q)
# start = proc.time()
# z_proj_q = MTD_projection_quad_prog(lag,Nc,p,q,Z)
# time_quad_prog = proc.time() - start
# start = proc.time()
# z_proj_z = zykstra_projection(lag,Nc,p,q,Z)
# time_zyk = proc.time() - start
# #print(z_proj_q)
# print(max(abs(z_proj_z - z_proj_q)))
# 
# print(time_zyk)
# print(time_quad_prog)
set.seed(23)
lag = 1
q = 1
time_zyk = array(0,c(length(pset),reps))
time_quad_prog = array(0,c(length(pset),reps))
diff = array(0,c(length(pset),reps))
sd = 1
for (p in 1:length(pset)) {
  #for (sd in 1:length(noise)) {
    for (i in 1:reps) {
      print(c(p,sd,i))
      Nc = rep(3,pset[p])
      Z = random_initialize_variables_MTD(Nc,pset[p],lag,q,noise[sd])      
      start = proc.time()
      z_proj_q = MTD_projection_quad_prog(lag,Nc,pset[p],q,Z)
      temp = proc.time() - start
      time_quad_prog[p,i] = temp[3]
      zykstra_param = create_matrices_for_projection(Nc,lag,pset[p],q)
      zykstra_param[[3]] = create_inds_for_Z_vectorization(Nc,pset[p],q)
      start = proc.time()
      z_proj_z = zykstra_projection(lag,Nc,pset[p],q,Z,zykstra_param)
      temp = proc.time() - start
      time_zyk[p,i] = temp[3]
      diff[p,i] = max(abs(z_proj_z - z_proj_q))
      print(max(abs(z_proj_z - z_proj_q)))
      #print(max(abs(z_proj_z - z_proj_q)))
    }
  #}
}

tz = apply(time_zyk,c(1),mean)
tq = apply(time_quad_prog,c(1),mean)

minmy = min(min(tz),min(tq))
maxmy = max(max(tz),max(tq))

pdf("/Users/alextank/Documents/granger_categorical_doc/untitled folder/compare_projection_2.pdf")
plot(pset,tq,type="o",col="blue",ylim=c(minmy,maxmy),ylab="Run Time (s)", xlab="d",main = "Run Time Comparison",lwd=1.6,cex.axis=1.5,cex.lab=1.5,cex.main=1.5)
lines(pset,tz,type="o",col="red",lwd=1.4)
legend(10, 2.5, legend=c("quadprog", "Dykstra"),
       col=c("blue", "red"), cex=1.3,lwd=1.4)
dev.off()

pdf("/Users/alextank/Documents/granger_categorical_doc/untitled folder/zykstra_2.pdf")
plot(pset,tz,type="o",col="red",ylim=c(min(tz),max(tz)),ylab="Run Time (s)", xlab="d",main="Run Time for Dykstra Projection",lwd=1.6,cex.axis=1.5,cex.lab=1.5,cex.main=1.5)
dev.off()

